Pinterest
---------

Description
-----------

Pinterest is a module provides a way for a Drupal site to interact with the Pinterest Site and 
incorporate Pinterest images, descriptions, and links. Currently it provides a single
Pinterest Block that can be configured to show the latest images a Pinterest user has pinned. Also, you
can configure the Pinterest board of the user from which you want the pins to be shown. 

For a full description of the module, visit the project page:
  http://drupal.org/project/pinterest
  
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/pinterest
  
-- REQUIREMNETS --
None.


-- INSTALLATION --
* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --
* Configure the module in Configuration  -> Pinterest settings
OR You can directly go to admin/config/pinterest 

1) User ID:
    Set this to the user id of the pins you want to display
	
2) User's Board Name:
    Give the name of the board of the pinterest user from which you want the pins to be displayed. If you leave this
    field blank, pins from the pinterest user id are displayed irrespective of the board.

3) Number of Pins:
    Set this to the number of thumbnails to display in the block.

Settings for the block:
1) Go to admin/structure/block and find the "Pinterest Pins" block. Set the region for this block where you want the pins to be displayed.


-- CONTACT --

Current maintainers:
* Matt West - http://drupal.org/user/65512/
* Ayush Jain - http://drupal.org/user/1744886/ (ayushjn)

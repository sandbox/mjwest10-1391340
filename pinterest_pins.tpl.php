<?php
/**
 * @file
 * Default theme implementation to display a list of Pinterst pin thumbnails.
 *
 * Available variables:
 * - $pins: An array of pins to display.
 *
 * Each $pin in $pins contains:
 * - $pin['link_url']: The link to the main post on Pinterest.
 * - $pin['image_url']: The url of the thumbnail image for the pin.
 * - $pin['description']: The Pinterest description for the pin.
 */
?>

<div class="pinterest-pinthumbs">
  <?php foreach ($pins as $pin): ?>       
        <a href="<?php print $pin['link_url'] ?>" target='_blank'><img class="pinterest-pinthumb"
            src="<?php print $pin['image_url'] ?>"
            alt="<?php print t('Pin Thumbnail') ?>"
            title="<?php print $pin['description'] ?>"/></a>
  <?php endforeach; ?>
</div>
